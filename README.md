# proj3-JSA
Author: Mason Jones

Contact: masonj@uoregon.edu

Purpose: A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

How to run: Fork repo. Run Docker. Build the Dockerfile. Run the Dockerfile. Open 127.0.0.1:5000 in a browser (or Docker's IP if required). Interact with game.
